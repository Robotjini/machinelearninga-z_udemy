import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.impute import SimpleImputer

def visiblePrint(printStatement):
	print()
	print("---------------------")
	print(printStatement)
	print("---------------------")
	print()

dataset = pd.read_csv('Data.csv')
x = dataset.iloc[:, :-1].values #Get everything but last column
y = dataset.iloc[:, -1].values #Get last column

#print(x)
#print(y)

'''Replace missing data with mean of other data in same column'''
imputer = SimpleImputer(missing_values=np.nan, strategy='mean')
imputer.fit(x[:, 1:3]) #end of range is exluded in python so 1:3 instead of 1:2
x[:, 1:3] = imputer.transform(x[:, 1:3])

#print(x)

from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import OneHotEncoder
ct = ColumnTransformer(transformers=[('encoder', OneHotEncoder(), [0])], remainder='passthrough')
x = np.array(ct.fit_transform(x))

#print(x)

from sklearn.preprocessing import LabelEncoder
le = LabelEncoder()
y = le.fit_transform(y)

#print(y)

'''Split data set into train and test set first so there is no data leakage from trainset to test set.'''
from sklearn.model_selection import train_test_split
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=1) #last input is not necessary just to follow along
visiblePrint(x_train)
visiblePrint(x_test)
visiblePrint(y_train)
visiblePrint(y_test)

'''Feature Scaling'''
from sklearn.preprocessing import StandardScaler
sc = StandardScaler()
x_train[:, 3:] = sc.fit_transform(x_train[:, 3:])
x_test[:, 3:] = sc.transform(x_test[:, 3:]) #need to use the parameters found in train set so a new mean and standard deviation is not found

visiblePrint(x_train)
visiblePrint(x_test) 