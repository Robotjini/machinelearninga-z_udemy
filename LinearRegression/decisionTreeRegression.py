import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

def visiblePrint(printStatement):
	print()
	print("---------------------")
	print(printStatement)
	print("---------------------")
	print()

#dataset = pd.read_csv('Position_Salaries.csv')
dataset = pd.read_csv('final_data_test.csv')
X = dataset.iloc[:, :-1].values
y = dataset.iloc[:, -1].values

from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)

from sklearn.tree import DecisionTreeRegressor
regressor = DecisionTreeRegressor(random_state = 0)
regressor.fit(X_train, y_train)

#predicting test results
y_pred = regressor.predict(X_test)
np.set_printoptions(precision=2)
visiblePrint(np.concatenate((y_pred.reshape(len(y_pred),1), y_test.reshape(len(y_test),1)),1))

#evaluating model preformance
from sklearn.metrics import r2_score
visiblePrint("R2 score:" + str(r2_score(y_test, y_pred)))

'''X_grid = np.arange(min(x), max(x), 0.1)
X_grid = X_grid.reshape((len(X_grid), 1))
plt.scatter(x, y, color = 'red')
plt.plot(X_grid, clf.predict(X_grid), color = 'blue')
plt.title('Decision Tree Regression')
plt.xlabel('Position level')
plt.ylabel('Salary')
plt.show()'''